﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class PostService : IPostService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public IQueryable<PostViewModel> GetPostInfo(long? id)
        {
            var post = from a in db.Posts
                       join b in db.PostTypes on a.PostTypeId equals b.Id
                       where a.Id == id

                       select new PostViewModel()
                       {
                           Id = id,
                           Name = a.Name,
                           Description = a.Description,
                           Deadline = a.Deadline,
                           CreatedDate = a.CreatedDate,
                           PostType = b.Name
                       };

            return post;
        }

        public IQueryable<PostViewModel> GetPostsList()
        {
            var post = (from a in db.Posts
                       join b in db.PostTypes on a.PostTypeId equals b.Id

                       select new PostViewModel()
                       {
                           Id = a.Id,
                           Name = a.Name,
                           Description = a.Description,
                           Deadline = a.Deadline,
                           CreatedDate = a.CreatedDate,
                           PostType = b.Name
                       }).OrderByDescending(x => x.Id);

            return post;
        }

        public IQueryable<PostViewModel> GetNotificationList(long userid)
        {
            var post = (from a in db.Posts
                       join b in db.PostTypes on a.PostTypeId equals b.Id
                       where !db.ReadPosts.Any(x => x.PostId == a.Id && x.UserId == userid)

                       select new PostViewModel()
                       {
                           Id = a.Id,
                           Name = a.Name,
                           Description = a.Description,
                           Deadline = a.Deadline,
                           CreatedDate = a.CreatedDate,
                           PostType = b.Name
                       }).OrderByDescending(x => x.Id);

            return post;
        }

        public List<ProgramAndRankingViewModel> GetNotificationDetails(long? id, long clubid)
        {
            var post = from a in db.Posts
                       join b in db.PostTypes on a.PostTypeId equals b.Id
                       where a.Id == id

                       select new ProgramAndRankingViewModel()
                       {
                           Id = id,
                           Name = a.Name,
                           Description = a.Description,
                           Deadline = a.Deadline,
                           PostCreatedDate = a.CreatedDate,
                           PostType = b.Name
                       };

            var list = post.ToList();
            var sentProgram = db.JoinProgramLists.ToList();
            var sentRanking = db.RankingLists.ToList();

            foreach (var item in list)
            {
                var listProgramCreatedDate = sentProgram.Where(x => x.ClubId == clubid && x.PostId == item.Id).Select(x => x.CreatedDate).SingleOrDefault();
                var listRankingCreatedDate = sentRanking.Where(x => x.ClubId == clubid && x.PostId == item.Id).Select(x => x.RankedDate).SingleOrDefault();

                if (listProgramCreatedDate != null || listRankingCreatedDate != null)
                {
                    item.SentOrNot = "Đã gửi";
                }

                else
                {
                    item.SentOrNot = "Chưa gửi";
                }
            }

            return list;
        }

        public IQueryable<ClubsSentListViewModel> GetClubsSentList(long? id)
        {
            var programOrRanking = db.Posts.Where(x => x.Id == id).Select(x => x.PostTypeId).FirstOrDefault();

            if (programOrRanking == 1)
            {
                var list = from p in db.Posts
                           join j in db.JoinProgramLists on p.Id equals j.PostId
                           join c in db.Clubs on j.ClubId equals c.Id
                           where p.Id == id

                           select new ClubsSentListViewModel()
                           {
                               ClubId = j.ClubId,
                               ClubName = c.Name,
                               CreatedDate = j.CreatedDate,
                               Deadline = p.Deadline,
                               SentListId = j.Id,
                               PostTypeId = p.PostTypeId
                           };

                return list;
            }

            else
            {
                var list = from p in db.Posts
                           join r in db.RankingLists on p.Id equals r.PostId
                           join c in db.Clubs on r.ClubId equals c.Id
                           where p.Id == id

                           select new ClubsSentListViewModel()
                           {
                               ClubId = r.ClubId,
                               ClubName = c.Name,
                               CreatedDate = r.RankedDate,
                               Deadline = p.Deadline,
                               SentListId = r.Id,
                               PostTypeId = p.PostTypeId
                           };

                return list;
            }
        }

        public IQueryable<ListForProgramViewModel> GetListMemberForProgram(long? id)
        {
            var query = from j in db.JoinProgramDetails 
                        join m in db.Members on j.MemberId equals m.Id
                        where j.JoinProgramListId == id

                        select new ListForProgramViewModel()
                        {
                            JoinProgramListId = j.JoinProgramListId,
                            Id = j.MemberId,
                            Name = m.Name,
                            Phone = m.Phone,
                            Email = m.Email,
                            Class = m.Class,
                            Major = m.Major,
                            JoinedOrNot = j.JoinedOrNot
                        };

            return query;
        }

        public IQueryable<ListForRankingViewModel> GetListMemberForRanking(long? listid, long? clubid)
        {
            var query = from a in db.RankingLists
                        join e in db.RankingListDetails on a.Id equals e.RankingListId
                        join c in db.RankingCategories on e.RankingCategoryId equals c.Id
                        join b in db.JoinClubs on e.MemberId equals b.MemberId where b.ClubId == clubid
                        join m in db.Members on b.MemberId equals m.Id
                        join r in db.Ranks on b.RankId equals r.Id
                        join d in db.Departments on b.DepartmentId equals d.Id

                        where e.RankingListId == listid && a.ClubId == clubid

                        select new ListForRankingViewModel()
                        {
                            Id = e.MemberId,
                            Name = m.Name,
                            DateOfBirth = m.DateOfBirth,
                            Class = m.Class,
                            Major = m.Major,
                            DepartmentName = d.Name,
                            RankName = r.Name,
                            RankingCategory = c.Name,
                            Mark10 = e.Mark10,
                            Mark4 = e.Mark4,
                        };

            return query;
        }

        public void UpdateJoinedOrNot(long joinProgramListId, string memberId, byte? joinedOrNot)
        {

            (from j in db.JoinProgramDetails
             where j.JoinProgramListId == joinProgramListId && j.MemberId == memberId select j).ToList().ForEach(x => x.JoinedOrNot = joinedOrNot);
          
        }
    }
}