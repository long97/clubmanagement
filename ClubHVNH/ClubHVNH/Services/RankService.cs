﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class RankService : IRankService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public IQueryable<RankViewModel> GetRanksList(long clubid)
        {
            var rank = from r in db.Ranks
                       join c in db.Clubs on r.ClubId equals c.Id

                       where r.ClubId == clubid

                       select new RankViewModel()
                       {
                           Id = r.Id,
                           Name = r.Name,
                           ClubId = r.ClubId,
                           ClubName = c.Name
                       };

            return rank;
        }

        public IQueryable<RankViewModel> GetRankInfo(long id, long clubid)
        {
            var rank = from r in db.Ranks
                       join c in db.Clubs on r.ClubId equals c.Id

                       where r.ClubId == clubid && r.Id == id

                       select new RankViewModel
                       {
                           Id = r.Id,
                           Name = r.Name,
                           ClubId = r.ClubId,
                           ClubName = c.Name
                       };

            return rank;
        }

        public void UpdateRank(long id, string name)
        {
            (from r in db.Ranks where r.Id == id select r).ToList().ForEach(x =>
                                                                            {
                                                                                x.Name = name;
                                                                            });

            db.SaveChanges();
        }
    }
}