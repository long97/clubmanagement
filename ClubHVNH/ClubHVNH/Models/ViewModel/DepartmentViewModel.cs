﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class DepartmentViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Tên ban")]
        [StringLength(250)]
        public string Name { get; set; }

        public long? ClubId { get; set; }

        [Display(Name = "Tên câu lạc bộ")]
        public string ClubName { get; set; }

        [Display(Name = "Mô tả")]
        [StringLength(250)]
        public string Description { get; set; }
    }
}