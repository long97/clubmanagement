namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JoinProgramDetail")]
    public partial class JoinProgramDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long JoinProgramListId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string MemberId { get; set; }

        public byte? JoinedOrNot { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
    }
}
