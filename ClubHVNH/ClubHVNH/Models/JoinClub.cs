﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JoinClub")]
    public partial class JoinClub
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string MemberId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ClubId { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Tên ban")]
        public long? DepartmentId { get; set; }

        [Display(Name = "Chức vụ")]
        public long? RankId { get; set; }
    }
}
