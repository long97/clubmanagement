﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ClubHVNH.Services.Interfaces
{
    public interface IImportExcelService
    {
        /// <summary>
        /// Open excel file, read and save data from excel to database
        /// </summary>
        /// <param name="path">path of excel file</param>
        void ReadAndSaveDataFromExcel(string path);
    }
}
