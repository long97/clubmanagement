﻿using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IProgramService
    {
        List<ProgramAndRankingViewModel> GetProgramListView(long clubid);
        List<ListForProgramViewModel> GetListForProgram(long postid, long clubid);
    }
}
