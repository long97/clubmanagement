namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RankingList")]
    public partial class RankingList
    {
        public long Id { get; set; }

        public long? ClubId { get; set; }

        public long? PostId { get; set; }

        public DateTime? RankedDate { get; set; }
    }
}
