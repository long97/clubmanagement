namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RankingListDetail")]
    public partial class RankingListDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long RankingListId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string MemberId { get; set; }

        public long? RankingCategoryId { get; set; }

        public double? Mark10 { get; set; }

        public double? Mark4 { get; set; }
    }
}
