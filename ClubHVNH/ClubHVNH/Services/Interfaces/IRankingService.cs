﻿using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IRankingService
    {
        List<ProgramAndRankingViewModel> GetRankingListView(long clubid);
        List<ListForRankingViewModel> GetListForRanking(long postid, long clubid);

    }
}
