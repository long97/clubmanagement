using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using ClubHVNH.Services.Interfaces;
using ClubHVNH.Services;

namespace ClubHVNH
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IImportExcelService, ImportExcelService>();
            container.RegisterType<IMemberService, MemberService>();
            container.RegisterType<IRankService, RankService>();
            container.RegisterType<IDepartmentService, DepartmentService>();
            container.RegisterType<IPostService, PostService>();
            container.RegisterType<IListForProgramService, ListForProgramService>();
            container.RegisterType<IProgramService, ProgramService>();
            container.RegisterType<IRankingService, RankingService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}