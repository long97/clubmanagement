﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Member")]
    public partial class Member
    {
        [Display(Name = "Mã sinh viên")]
        [StringLength(50)]
        public string Id { get; set; }

        [Display(Name = "Họ và tên")]
        [StringLength(250)]
        public string Name { get; set; }

        [Display(Name = "Ngày sinh")]
        [StringLength(50)]
        public string DateOfBirth { get; set; }

        [Display(Name = "Lớp")]
        [StringLength(50)]
        public string Class { get; set; }

        [Display(Name = "Khoa")]
        [StringLength(250)]
        public string Major { get; set; }

        [Display(Name = "SĐT")]
        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }
    }
}
