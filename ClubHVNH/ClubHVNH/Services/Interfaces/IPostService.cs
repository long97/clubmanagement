﻿using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IPostService
    {
        IQueryable<PostViewModel> GetPostInfo(long? id);
        IQueryable<PostViewModel> GetPostsList();
        IQueryable<PostViewModel> GetNotificationList(long userid);
        IQueryable<ClubsSentListViewModel> GetClubsSentList(long? id);
        IQueryable<ListForProgramViewModel> GetListMemberForProgram(long? id);
        List<ProgramAndRankingViewModel> GetNotificationDetails(long? id, long clubid);
        IQueryable<ListForRankingViewModel> GetListMemberForRanking(long? listid, long? clubid);
        void UpdateJoinedOrNot(long joinProgramListId, string memberId, byte? joinedOrNot);
    }
}
