﻿using ClubHVNH.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ClubHVNH.Services.Interfaces;
using PagedList;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "HSV")]
    public class ImportExcelController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IImportExcelService importService;

        //inject dependency
        public ImportExcelController(IImportExcelService importService)
        {
            this.importService = importService;
        }

        // GET: Import
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentList(int? page)
        {
            // 1. Tham số int? dùng để thể hiện null và kiểu int
            // page có thể có giá trị là null và kiểu int.

            // 2. Nếu page = null thì đặt lại là 1.
            if (page == null) page = 1;

            // 3. Tạo truy vấn, lưu ý phải sắp xếp theo trường nào đó, ví dụ OrderBy
            // theo LinkID mới có thể phân trang.
            var list = db.Members.OrderBy(x => x.Id);

            // 4. Tạo kích thước trang (pageSize) hay là số Link hiển thị trên 1 trang
            int pageSize = 10;

            // 4.1 Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
            // nếu page = null thì lấy giá trị 1 cho biến pageNumber.
            int pageNumber = (page ?? 1);

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            if(excelfile == null || excelfile.ContentLength == 0)
            {
                ViewBag.Error = "Hãy chọn một file excel";
                return View("Index");
            }
            else
            {
                if(excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx"))
                {
                    string fileName = Path.GetFileName(excelfile.FileName);
                    string path = Path.Combine(Server.MapPath("~/Import/"), fileName);


                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    
                    excelfile.SaveAs(path);

                    importService.ReadAndSaveDataFromExcel(path);
                    
                    return RedirectToAction("StudentList");
                }
                else
                {
                    ViewBag.Error = "Loại file không đúng";
                    return View("Index");
                }
            }
        }
    }
}