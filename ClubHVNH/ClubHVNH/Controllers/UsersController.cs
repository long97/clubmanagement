﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services;
using ClubHVNH.Services.Interfaces;

namespace ClubHVNH.Controllers
{
    public class UsersController : Controller
    {              
        private ClubManagementDbContext db = new ClubManagementDbContext();

        private readonly IUserService userService;

        //inject dependency
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// Get data for DropDownList from Club table
        /// </summary>
        /// <returns></returns>
        public List<Club> ListAll()
        {
            return db.Clubs.ToList();
        }

        /// <summary>
        /// Set data for DropDownList
        /// </summary>
        /// <param name="selectedId"></param>
        public void SetViewBag(long? selectedId = null)
        {
            ViewBag.ClubId = new SelectList(ListAll(), "Id", "Name", selectedId);
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        // GET: Users
        public ActionResult Index()
        {
            return View(userService.GetUsersList());
        }

        // GET: Users/Details/5
        public ActionResult Details(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = userService.GetUsersInfo(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Create()
        {
            SetViewBag();
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,IsAdmin,Email,CreatedDate,ModifiedDate,ClubId")] User user)
        {
            if (ModelState.IsValid)
            {
                string encryptMD5Pass = Common.EncryptMD5(user.Username + user.Password);
                user.Password = encryptMD5Pass;

                var createdate = DateTime.Now;
                user.CreatedDate = createdate;

                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            SetViewBag();

            return View(user);
        }

        // GET: Users/Edit/5
        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);

            SetViewBag(user.ClubId);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email")] string email, long id, bool isadmin, long? ClubId)
        {
            if (ModelState.IsValid)
            {
                userService.UpdateUser(email, id, isadmin, ClubId);
                return RedirectToAction("Index");
            }

            return View("Edit");
        }

        public ActionResult EditPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditPassword(string username, string password)
        {
            if (ModelState.IsValid)
            {
                userService.UpdateNewPassword(username, password);

                if(Convert.ToInt32(Session["isadmin"]) == 1)
                {
                    return RedirectToAction("IndexAdmin", "Home");
                }

                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
            }

            return View("EditPassword");
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        // GET: Users/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = userService.GetUsersInfo(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            User user = db.Users.Find(id);

            db.Users.Remove(user);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult SendNewPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendNewPassword(string username)
        {
            var user = db.Users.SingleOrDefault(x => x.Username == username);

            if (user != null)
            {
                var email = user.Email;
                var password = userService.RandomPassword();

                userService.UpdateNewPassword(user.Username, password);

                userService.SendNewPassword(email, password, user.Username);

                ViewBag.message = "Gửi mật khẩu mới thành công! Kiểm tra Email của bạn và đăng nhập lại";

                return View("~/Views/Home/Login.cshtml");
            }

            ViewBag.error = "Không tồn tại tên đăng nhập này";

            return View();
        }

        /// <summary>
        /// Check if Club name is exists in Database
        /// </summary>
        /// <param name="Name">Club name is entered on the form</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult IsUserExists(string Username)
        {
            if (userService.IsUserExists(Username) == true)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
