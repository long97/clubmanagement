﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "CLB")]
    public class ListForProgramController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IMemberService memberService;
        private readonly IProgramService programService;

        //inject dependency
        public ListForProgramController(IMemberService memberService, IProgramService programService)
        {
            this.memberService = memberService;
            this.programService = programService;
        }

        // GET: ListForProgram
        public ActionResult Index()
        {
            var clubid = (long)Session["clubid"];
            var model = programService.GetProgramListView(clubid);

            return View(model);
        }

        public ActionResult Create()
        {
            var clubid = (long)Session["clubid"];
            var model = memberService.GetMembersList(clubid);

            return View(model);
        }

        [HttpPost]
        public JsonResult Create(List<JoinProgramDetail> joinProgramDetails)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            var joinProgramList = new JoinProgramList();

            long postid = Convert.ToInt32(RouteData.Values["id"]);

            string message = "ok";

            //Check for NULL.
            if (joinProgramDetails == null)
            {
                joinProgramDetails = new List<JoinProgramDetail>();
            }


            joinProgramList.ClubId = clubid;
            joinProgramList.PostId = postid;
            joinProgramList.CreatedDate = DateTime.Now;

            db.JoinProgramLists.Add(joinProgramList);
            db.SaveChanges();

            var listid = joinProgramList.Id;

            //Loop and insert records.
            foreach (var joinProgramDetail in joinProgramDetails)
            {
                var memberIsValid = db.Members.Find(joinProgramDetail.MemberId);

                if (memberIsValid == null)
                {
                    message = joinProgramDetail.MemberId;
                    break;
                }

                joinProgramDetail.JoinProgramListId = listid;

                db.JoinProgramDetails.Add(joinProgramDetail);
            }

            db.SaveChanges();

            return Json(message);
        }

        public ActionResult ShowListForProgram(long id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            return View(programService.GetListForProgram(id, clubid));
        }
    }
}