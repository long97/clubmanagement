﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace ClubHVNH.Controllers
{
    public class HomeController : Controller
    {
        ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IPostService postService;

        //inject dependency
        public HomeController(IPostService postService)
        {
            this.postService = postService;
        }

        [AuthorizeUser(AccessLevel = "CLB")]
        public ActionResult Index(int? page)
        {
            // 1. Tham số int? dùng để thể hiện null và kiểu int
            // page có thể có giá trị là null và kiểu int.

            // 2. Nếu page = null thì đặt lại là 1.
            if (page == null) page = 1;

            // 3. Tạo truy vấn, lưu ý phải sắp xếp theo trường nào đó, ví dụ OrderBy
            // theo LinkID mới có thể phân trang.
            var list = postService.GetPostsList();

            // 4. Tạo kích thước trang (pageSize) hay là số Link hiển thị trên 1 trang
            int pageSize = 7;

            // 4.1 Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
            // nếu page = null thì lấy giá trị 1 cho biến pageNumber.
            int pageNumber = (page ?? 1);

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult IndexAdmin()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult NotiUserPartial()
        {
            long userid;

            if(Session["userid"] != null)
            {
                userid = (long)Session["userid"];
            }

            else
            {
                userid = -1;
            }

            return PartialView(postService.GetNotificationList(userid));
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            string passwordMD5 = Common.EncryptMD5(username + password);
            var user = db.Users.SingleOrDefault(x => x.Username == username && x.Password == passwordMD5);

            if (user != null)
            {
                Session["userid"] = user.Id;
                Session["username"] = user.Username;
                Session["isadmin"] = user.IsAdmin;
                Session["clubid"] = user.ClubId;

                if (!Convert.ToBoolean(Session["isadmin"]))
                {
                    return RedirectToAction("Index");
                }

                else
                {
                    return RedirectToAction("IndexAdmin");
                }
                
            }

            ViewBag.error = "Đăng nhập sai hoặc bạn không có quyền vào";

            return View();
        }

        public ActionResult Logout()
        {
            Session["userid"] = null;
            Session["username"] = null;
            Session["isadmin"] = null;
            Session["clubid"] = null;

            return RedirectToAction("Index");
        }

        public ActionResult AuthorizeAlert()
        {
            return View();
        }
    }
}