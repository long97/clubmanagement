﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class ProgramService : IProgramService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public List<ProgramAndRankingViewModel> GetProgramListView(long clubid)
        {

            var query = (from a in db.Posts
                       join b in db.PostTypes on a.PostTypeId equals b.Id
                       where b.Id == 1

                       select new ProgramAndRankingViewModel()
                       {
                           Id = a.Id,
                           Name = a.Name,
                           Description = a.Description,
                           Deadline = a.Deadline,
                           PostCreatedDate = a.CreatedDate,
                           PostType = b.Name
                       }).OrderByDescending(x => x.Id);

            var list = query.ToList();

            var joinlist = db.JoinProgramLists.ToList();

            foreach (var item in list)
            {                
                var listCreatedDate = joinlist.Where(x => x.ClubId == clubid && x.PostId == item.Id).Select(x => x.CreatedDate).SingleOrDefault();

                if (listCreatedDate != null)
                {
                    TimeSpan deadLineRange = Convert.ToDateTime(item.Deadline) - Convert.ToDateTime(listCreatedDate);

                    int deadLineValue = Convert.ToInt16(deadLineRange.TotalHours);

                    if (deadLineValue >= 0)
                    {
                        item.SentOrNot = "Gửi đúng hạn";
                    }

                    else
                    {
                        if (deadLineValue <= 0)
                        {
                            item.SentOrNot = "Gửi muộn";
                        }
                    }
                }

                else
                {
                    item.SentOrNot = "Chưa gửi";
                }
                
            }
            
            return list;
        }

        public List<ListForProgramViewModel> GetListForProgram(long postid, long clubid)
        {
            var query = from j in db.JoinProgramLists
                        join a in db.JoinProgramDetails on j.Id equals a.JoinProgramListId
                        join b in db.JoinClubs on a.MemberId equals b.MemberId where b.ClubId == clubid
                        join m in db.Members on b.MemberId equals m.Id
                        join r in db.Ranks on b.RankId equals r.Id
                        join d in db.Departments on b.DepartmentId equals d.Id

                        where j.ClubId == clubid && j.PostId == postid

                        select new ListForProgramViewModel()
                        {
                            Id = a.MemberId,
                            Name = m.Name,
                            DepartmentName = d.Name,
                            RankName = r.Name,
                            JoinedOrNot = a.JoinedOrNot
                        };

            return query.ToList();

        }
    }
}