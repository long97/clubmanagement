﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IUserService
    {
        IQueryable<UserViewModel> GetUsersInfo(long? id);
        IQueryable<UserViewModel> GetUsersList();
        void SendNewPassword(string email, string password, string username);
        void UpdateNewPassword(string username, string password);
        int RandomNumber(int min, int max);
        string RandomString(int size, bool lowerCase);
        string RandomPassword();
        void UpdateUser(string email, long id, bool isadmin, long? ClubId);
        bool IsUserExists(string Username);
    }
}
