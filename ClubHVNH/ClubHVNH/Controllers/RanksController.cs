﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "CLB")]
    public class RanksController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IRankService rankService;

        //inject dependency
        public RanksController(IRankService rankService)
        {
            this.rankService = rankService;
        }

        // GET: Ranks
        public ActionResult Index()
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            return View(rankService.GetRanksList(clubid));
        }

        // GET: Ranks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ranks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Rank rank)
        {
            if (ModelState.IsValid)
            {
                rank.ClubId = (long)Session["clubid"]; //Get ClubId from Session
                db.Ranks.Add(rank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rank);
        }

        // GET: Ranks/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rank rank = db.Ranks.Find(id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        // POST: Ranks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] long id, string Name)
        {
            if (ModelState.IsValid)
            {
                rankService.UpdateRank(id, Name);

                return RedirectToAction("Index");
            }
            return View("Edit");
        }

        // GET: Ranks/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rank rank = db.Ranks.Find(id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        // POST: Ranks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Rank rank = db.Ranks.Find(id);
            db.Ranks.Remove(rank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}