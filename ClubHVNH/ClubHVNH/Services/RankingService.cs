﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class RankingService : IRankingService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public List<ProgramAndRankingViewModel> GetRankingListView(long clubid)
        {

            var query = (from a in db.Posts
                        join b in db.PostTypes on a.PostTypeId equals b.Id
                        where b.Id == 2

                        select new ProgramAndRankingViewModel()
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Description = a.Description,
                            Deadline = a.Deadline,
                            PostCreatedDate = a.CreatedDate,
                            PostType = b.Name
                        }).OrderByDescending(x => x.Id);

            var list = query.ToList();

            var rankinglist = db.RankingLists.ToList();

            foreach (var item in list)
            {
                var listCreatedDate = rankinglist.Where(x => x.ClubId == clubid && x.PostId == item.Id).Select(x => x.RankedDate).SingleOrDefault();

                if (listCreatedDate != null)
                {
                    TimeSpan deadLineRange = Convert.ToDateTime(item.Deadline) - Convert.ToDateTime(listCreatedDate);

                    int deadLineValue = Convert.ToInt16(deadLineRange.TotalHours);

                    if (deadLineValue >= 0)
                    {
                        item.SentOrNot = "Gửi đúng hạn";
                    }

                    else
                    {
                        if (deadLineValue <= 0)
                        {
                            item.SentOrNot = "Gửi muộn";
                        }
                    }
                }

                else
                {
                    item.SentOrNot = "Chưa gửi";
                }

            }

            return list;
        }

        public List<ListForRankingViewModel> GetListForRanking(long postid, long clubid)
        {
            var query = from j in db.RankingLists
                        join a in db.RankingListDetails on j.Id equals a.RankingListId
                        join c in db.RankingCategories on a.RankingCategoryId equals c.Id
                        join b in db.JoinClubs on a.MemberId equals b.MemberId where b.ClubId == clubid
                        join m in db.Members on b.MemberId equals m.Id
                        join r in db.Ranks on b.RankId equals r.Id
                        join d in db.Departments on b.DepartmentId equals d.Id

                        where j.ClubId == clubid && j.PostId == postid

                        select new ListForRankingViewModel()
                        {
                            Id = a.MemberId,
                            Name = m.Name,
                            DateOfBirth = m.DateOfBirth,
                            Class = m.Class,
                            Major = m.Major,
                            DepartmentName = d.Name,
                            RankName = r.Name,
                            RankingCategory = c.Name,
                            Mark10 = a.Mark10,
                            Mark4 = a.Mark4
                        };

            return query.ToList();

        }
    }
}