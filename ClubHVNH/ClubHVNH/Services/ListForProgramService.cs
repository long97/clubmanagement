﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class ListForProgramService : IListForProgramService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public IEnumerable<ListForProgramViewModel> GetCreateListForProgramView(long clubid, long postid)
        {

            //Searching records from list using LINQ query  
            var list = from j in db.JoinClubs.AsEnumerable()
                             join m in db.Members.AsEnumerable() on j.MemberId equals m.Id
                             join d in db.Departments.AsEnumerable() on j.DepartmentId equals d.Id
                             join r in db.Ranks.AsEnumerable() on j.RankId equals r.Id

                             where j.ClubId == clubid

                             select new ListForProgramViewModel()
                             {
                                 Id = m.Id,
                                 Name = m.Name,
                                 DepartmentName = d.Name,
                                 RankName = r.Name,
                                 PostId = postid
                             };

            return list.ToList();
        }
    }
}