﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "CLB")]
    public class ListForRankingController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IMemberService memberService;
        private readonly IRankingService rankingService;

        //inject dependency
        public ListForRankingController(IMemberService memberService, IRankingService rankingService)
        {
            this.memberService = memberService;
            this.rankingService = rankingService;
        }

        // GET: ListForRanking
        public ActionResult Index()
        {
            var clubid = (long)Session["clubid"];
            var model = rankingService.GetRankingListView(clubid);

            return View(model);
        }

        public ActionResult Create()
        {
            var clubid = (long)Session["clubid"];
            var model = memberService.GetMembersList(clubid);

            ViewBag.RankingCategoryId = new SelectList(db.RankingCategories, "Id", "Name");

            return View(model);
        }

        [HttpPost]
        public JsonResult Create(List<RankingListDetail> rankingListDetails)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            var rankingList = new RankingList();

            long postid = Convert.ToInt32(RouteData.Values["id"]);

            string message = "ok";

            //Check for NULL.
            if (rankingListDetails == null)
            {
                rankingListDetails = new List<RankingListDetail>();
            }


            rankingList.ClubId = clubid;
            rankingList.PostId = postid;
            rankingList.RankedDate = DateTime.Now;

            db.RankingLists.Add(rankingList);
            db.SaveChanges();

            var listid = rankingList.Id;

            //Loop and insert records.
            foreach (var rankingListDetail in rankingListDetails)
            {
                var memberIsValid = db.Members.Find(rankingListDetail.MemberId);

                if (memberIsValid == null)
                {
                    message = rankingListDetail.MemberId;
                    break;
                }

                rankingListDetail.RankingListId = listid;
                rankingListDetail.Mark10 = Convert.ToDouble(rankingListDetail.Mark10);
                rankingListDetail.Mark4 = Convert.ToDouble(rankingListDetail.Mark4);

                db.RankingListDetails.Add(rankingListDetail);
            }

            db.SaveChanges();

            return Json(message);
        }

        public ActionResult ShowListForRanking(long id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            return View(rankingService.GetListForRanking(id, clubid));
        }
    }
}