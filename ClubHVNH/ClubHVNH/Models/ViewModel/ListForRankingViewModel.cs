﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class ListForRankingViewModel
    {
        [Display(Name = "Mã sinh viên")]
        public string Id { get; set; }

        [Display(Name = "Họ và tên")]
        public string Name { get; set; }

        [Display(Name = "Ngày sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Lớp")]
        public string Class { get; set; }

        [Display(Name = "Khoa")]
        public string Major { get; set; }

        [Display(Name = "SĐT")]
        public string Phone { get; set; }

        public string Email { get; set; }

        [Display(Name = "Tên ban")]
        public string DepartmentName { get; set; }

        [Display(Name = "Chức vụ")]
        public string RankName { get; set; }

        [Display(Name = "Mức độ đánh giá")]
        public string RankingCategory { get; set; }

        [Display(Name = "Điểm hệ 10")]
        public double? Mark10 { get; set; }

        [Display(Name = "Điểm hệ 4")]
        public double? Mark4 { get; set; }

    }
}