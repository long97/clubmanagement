﻿using ClubHVNH.Controllers;
using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Services
{
    public class UserService : IUserService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        //public object MessageBox { get; private set; }

        public IQueryable<UserViewModel> GetUsersInfo(long? id)
        {
            var user = from u in db.Users
                       join c in db.Clubs on u.ClubId equals c.Id
                       where u.Id == id
                       select new UserViewModel()
                       {
                           Id = id,
                           IsAdmin = u.IsAdmin,
                           ClubId = c.Id,
                           ClubName = c.Name,
                           Email = u.Email,
                           Username = u.Username,
                           Password = u.Password,
                           CreatedDate = u.CreatedDate,
                           ModifiedDate = u.ModifiedDate
                       };

            return user;
        }

        public IQueryable<UserViewModel> GetUsersList()
        {
            var user = from u in db.Users
                       join c in db.Clubs on u.ClubId equals c.Id
                       select new UserViewModel
                       {
                           Id = u.Id,
                           IsAdmin = u.IsAdmin,
                           ClubId = c.Id,
                           ClubName = c.Name,
                           Email = u.Email,
                           Username = u.Username,
                           Password = u.Password,
                           CreatedDate = u.CreatedDate,
                           ModifiedDate = u.ModifiedDate
                       };

            return user;
        }

        public void SendNewPassword(string email, string password, string username)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();
            var toEmailAddress = email;

            bool enableSsl = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"].ToString());

            string body = "Xin chào " + username + "! Mật khẩu mới cho tài khoản của bạn là: " + password;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = "Thông báo lấy lại mật khẩu";
            message.IsBodyHtml = true;
            message.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enableSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(message);
        }

        public void UpdateNewPassword(string username, string password)
        {
            string encryptMD5Pass = Common.EncryptMD5(username + password);

            (from u in db.Users where u.Username == username select u).ToList()
                                                                       .ForEach(x => x.Password = encryptMD5Pass);

            db.SaveChanges();
        }

        // Generate a random number between two numbers
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        // Generate a random string with a given size  
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        // Generate a random password  
        public string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        public void UpdateUser(string email, long id, bool isadmin, long? ClubId)
        {
            (from u in db.Users where u.Id == id select u).ToList().ForEach(x =>
                                                                                {
                                                                                    x.Email = email;
                                                                                    x.ModifiedDate = DateTime.Now;
                                                                                    x.ClubId = ClubId;
                                                                                    x.IsAdmin = isadmin;
                                                                                });

            db.SaveChanges();
        }

        /// <summary>
        /// Check if Username already exists
        /// </summary>
        /// <param name="Username"></param>
        /// <returns></returns>
        public bool IsUserExists(string Username)
        {
            var validateName = db.Users.FirstOrDefault(x => x.Username == Username);

            if (validateName != null)
            {
                return true;
            }

            return false;

        }
    }
};