﻿using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IRankService
    {
        IQueryable<RankViewModel> GetRanksList(long clubid);
        IQueryable<RankViewModel> GetRankInfo(long id, long clubid);
        void UpdateRank(long id, string name);
    }
}
