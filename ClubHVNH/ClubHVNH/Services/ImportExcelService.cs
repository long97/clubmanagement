﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using OfficeOpenXml;
using System.IO;

namespace ClubHVNH.Services
{
    public class ImportExcelService : IImportExcelService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        /// <summary>
        /// Open excel file, read and save data from excel to database
        /// </summary>
        /// <param name="path">path of excel file</param>
        public void ReadAndSaveDataFromExcel(string path)
        {

            //Microsoft.Office.Interop.Excel.Application appication = new Excel.Application();
            //Microsoft.Office.Interop.Excel.Workbook workbook = appication.Workbooks.Open(path);
            //Excel.Worksheet worksheet = workbook.ActiveSheet;
            //Excel.Range range = worksheet.UsedRange;

            //db.Database.ExecuteSqlCommand("TRUNCATE TABLE [Member]");

            //for (int row = 2; row <= range.Rows.Count; row++)
            //{
            //    var member = new Member();

            //    member.Id = ((Excel.Range)range.Cells[row, 1]).Text;
            //    member.Name = ((Excel.Range)range.Cells[row, 2]).Text;
            //    member.DateOfBirth = ((Excel.Range)range.Cells[row, 3]).Text;
            //    member.Class = ((Excel.Range)range.Cells[row, 4]).Text;
            //    member.Major = ((Excel.Range)range.Cells[row, 5]).Text;
            //    member.Phone = ((Excel.Range)range.Cells[row, 6]).Text;
            //    member.Email = ((Excel.Range)range.Cells[row, 7]).Text;

            //    db.Members.Add(member);

            //}

            //workbook.Close(0);
            //db.SaveChanges();

            byte[] bin = File.ReadAllBytes(path);

            //create a new Excel package in a memorystream
            MemoryStream stream = new MemoryStream(bin);
            ExcelPackage excelPackage = new ExcelPackage(stream);
            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1];

            //Truncate all old data in Member table
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE [Member]");

            //loop all rows
            for (int i = 2; i <= worksheet.Dimension.End.Row; i++)
            {
                var member = new Member();

                member.Id = worksheet.Cells[i, 1].Text;
                member.Name = worksheet.Cells[i, 2].Text;
                member.DateOfBirth = worksheet.Cells[i, 3].Text;
                member.Class = worksheet.Cells[i, 4].Text;
                member.Major = worksheet.Cells[i, 5].Text;
                member.Phone = worksheet.Cells[i, 6].Text;
                member.Email = worksheet.Cells[i, 7].Text;

                db.Members.Add(member);
            }

            db.SaveChanges();
        }
    }
}