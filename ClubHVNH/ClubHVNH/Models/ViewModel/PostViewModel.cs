﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class PostViewModel
    {
        public long? Id { get; set; }

        [Display(Name = "Tiêu đề")]
        public string Name { get; set; }

        [Display(Name = "Nội dung")]
        public string Description { get; set; }

        [Display(Name = "Hạn nộp danh sách")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Loại thông báo")]
        public string PostType { get; set; }
    }
}