﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "CLB")]
    public class MembersController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IMemberService memberService;

        //inject dependency
        public MembersController(IMemberService memberService)
        {
            this.memberService = memberService;
        }

        // GET: Members
        public ActionResult Index(int? page)
        {
            var clubid = (long)Session["clubid"];

            // 1. Tham số int? dùng để thể hiện null và kiểu int
            // page có thể có giá trị là null và kiểu int.

            // 2. Nếu page = null thì đặt lại là 1.
            if (page == null) page = 1;

            // 3. Tạo truy vấn, lưu ý phải sắp xếp theo trường nào đó, ví dụ OrderBy
            // theo LinkID mới có thể phân trang.
            var list = memberService.GetMembersList(clubid);

            // 4. Tạo kích thước trang (pageSize) hay là số Link hiển thị trên 1 trang
            int pageSize = 7;

            // 4.1 Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
            // nếu page = null thì lấy giá trị 1 cho biến pageNumber.
            int pageNumber = (page ?? 1);

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            ViewBag.RankId = new SelectList(db.Ranks.Where(r => r.ClubId == clubid), "Id", "Name");
            ViewBag.DepartmentId = new SelectList(db.Departments.Where(r => r.ClubId == clubid), "Id", "Name");

            return View();
        }

        [HttpPost]
        public JsonResult GetAutoCompleteSearch(string Prefix)
        {

            var memberList = memberService.GetMemberByPrefix(Prefix);

            return Json(memberList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSearchingData(string SearchValue)
        {

            var memberList = memberService.GetMemberById(SearchValue);

            return Json(memberList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertMembers(List<JoinClub> members)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];
            string message = "ok";

            //Check for NULL.
            if (members == null)
            {
                members = new List<JoinClub>();
            }

            //Loop and insert records.
            foreach (var member in members)
            {
                var memberExist = db.JoinClubs.Find(member.MemberId, clubid);
                var memberIsValid = db.Members.Find(member.MemberId);

                if(memberExist != null)
                {
                    message = member.MemberId;
                    break;
                }

                if(memberIsValid == null)
                {
                    message = member.MemberId;
                    break;
                }

                member.CreatedDate = DateTime.Now;
                member.ClubId = clubid;
                               
                db.JoinClubs.Add(member);
            }

            db.SaveChanges();

            return Json(message);

        }

        // GET: Members/Details/19A4040xxx
        public ActionResult Details(string id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var member = memberService.GetMembersInfo(clubid, id);

            if (member == null)
            {
                return HttpNotFound();
            }

            return View(member);
        }

        // GET: Members/Delete/19A4040xxx
        public ActionResult Delete(string id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var member = memberService.GetMembersInfo(clubid, id);

            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/19A4040xxx
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            JoinClub member = db.JoinClubs.Find(id, clubid);

            db.JoinClubs.Remove(member);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        // GET: Members/Edit/19A4040xxx
        public ActionResult Edit(string id)
        {
            //Get clubId from Session
            var clubid = (long)Session["clubid"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JoinClub member = db.JoinClubs.Find(id, clubid);

            ViewBag.RankId = new SelectList(db.Ranks.Where(r => r.ClubId == clubid), "Id", "Name", member.RankId);
            ViewBag.DepartmentId = new SelectList(db.Departments.Where(r => r.ClubId == clubid), "Id", "Name", member.DepartmentId);

            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Edit/19A4040xxx
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MemberId, DepartmentId, RankId")] string memberId, long? departmentId, long? rankId)
        {
            //Get clubId from Session
            var clubId = (long)Session["clubid"];

            if (ModelState.IsValid)
            {
                memberService.UpdateMember(memberId, clubId, departmentId, rankId);
                return RedirectToAction("Index");
            }

            return View("Edit");
        }

    }
}