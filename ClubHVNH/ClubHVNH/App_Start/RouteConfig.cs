﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ClubHVNH
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ListMemberForRanking",
               "{controller}/{action}/{listid}/{clubid}",
               new { controller = "Post", action = "ListMemberForRanking", listid = UrlParameter.Optional, clubid = UrlParameter.Optional }
            );
        }
    }
}
