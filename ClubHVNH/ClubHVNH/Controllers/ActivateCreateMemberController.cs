﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    [AuthorizeUser(AccessLevel = "HSV")]
    public class ActivateCreateMemberController : Controller
    {
        // GET: ActivateCreateMember
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Activate()
        {
            Session["active"] = true;

            return RedirectToAction("Index");
        }

        public ActionResult Deactivate()
        {
            Session["active"] = false;

            return RedirectToAction("Index");
        }
    }
}