﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class DepartmentService : IDepartmentService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public IQueryable<DepartmentViewModel> GetDepartmentsList(long clubid)
        {
            var department = from r in db.Departments
                       join c in db.Clubs on r.ClubId equals c.Id

                       where r.ClubId == clubid

                       select new DepartmentViewModel()
                       {
                           Id = r.Id,
                           Name = r.Name,
                           ClubId = r.ClubId,
                           ClubName = c.Name,
                           Description = r.Description
                       };

            return department;
        }

        public IQueryable<DepartmentViewModel> GetDepartmentInfo(long id, long clubid)
        {
            var department = from r in db.Departments
                       join c in db.Clubs on r.ClubId equals c.Id

                       where r.ClubId == clubid && r.Id == id

                       select new DepartmentViewModel
                       {
                           Id = r.Id,
                           Name = r.Name,
                           ClubId = r.ClubId,
                           Description = r.Description,
                           ClubName = c.Name
                       };

            return department;
        }

        public void UpdateDepartment(long id, string name, string description)
        {
            (from r in db.Departments where r.Id == id select r).ToList().ForEach(x =>
            {
                x.Name = name;
                x.Description = description;
            });

            db.SaveChanges();
        }
    }
}