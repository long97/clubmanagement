﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Post")]
    public partial class Post
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Hãy nhập tiêu đề")]
        [Display(Name = "Tiêu đề")]
        [StringLength(500)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Hãy nhập nội dung")]
        [Display(Name = "Nội dung")]
        [StringLength(2000)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Hãy nhập Deadline nộp danh sách")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Hạn nộp danh sách")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Loại thông báo")]
        public long? PostTypeId { get; set; }
    }
}