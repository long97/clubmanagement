﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("Club")]
    public partial class Club
    {
        public long Id { get; set; }
       
        [Required(ErrorMessage = "Hãy nhập tên câu lạc bộ")]
        [Display(Name = "Tên câu lạc bộ")]
        [StringLength(250)]
        [Remote("IsClubExists", "Clubs", ErrorMessage = "Câu lạc bộ này đã có")]
        public string Name { get; set; }
    }
}
