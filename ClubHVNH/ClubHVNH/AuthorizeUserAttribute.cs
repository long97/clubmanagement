﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ClubHVNH
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        public string AccessLevel { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            string isadmin = Convert.ToString(HttpContext.Current.Session["isadmin"]);

            string privilegeLevels; 

            if(isadmin == "True")
            {
                privilegeLevels = "HSV";
            }

            else
            {
                privilegeLevels = "CLB";
            }

            if(this.AccessLevel == privilegeLevels)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "Home",
                                action = "AuthorizeAlert"
                            })
                        );
        }

    }
}