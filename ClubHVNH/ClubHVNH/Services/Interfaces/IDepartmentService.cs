﻿using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IDepartmentService
    {
        IQueryable<DepartmentViewModel> GetDepartmentsList(long clubid);
        IQueryable<DepartmentViewModel> GetDepartmentInfo(long id, long clubid);
        void UpdateDepartment(long id, string name, string description);
    }
}
