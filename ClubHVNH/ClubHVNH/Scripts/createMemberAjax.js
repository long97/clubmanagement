﻿//Show suggest list of MemberId when we type on #txtId textbox
$("#txtId").autocomplete({
    maxShowItems: 8,
    source: function (request, response) {
        $.ajax({
            url: '@Url.Action("GetAutoCompleteSearch", "Members")',
            type: "POST",
            dataType: "json",
            data: { Prefix: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return { label: item.Id + " - " + item.Name, value: item.Id, data: item };

                }));
            }
        });
    },

    select: function (e, ui) {

        alert("selected!");

    }
});

$("body").on("click", "#btnAdd", function () {

    var error = '';
    $('#txtName').each(function () {
        if ($(this).val() == '') {

            error += "<p>Hãy nhập thông tin</p>";

            return false;
        }
    });

    if (error == '') {
        //Reference the Name and Country TextBoxes.
        var Id = $("#txtId");
        var Name = $("#txtName");
        var DateOfBirth = $("#txtDateOfBirth");
        var Class = $("#txtClass");
        var Major = $("#txtMajor");
        var DepartmentId = $("#DepartmentId");
        var RankId = $("#RankId");
        var DepartmentName = $("#DepartmentId option:selected");
        var RankName = $("#RankId option:selected");


        ////Get the reference of the Table's TBODY element.
        //var tBody = $("#tblMembers > TBODY")[0];

        ////Add Row.
        //var row = tBody.insertRow(-1);

        ////Add Name cell.
        //var cell = $(row.insertCell(-1));
        //cell.html(Id.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(Name.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(DateOfBirth.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(Class.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(Major.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(DepartmentId.val());

        //var cell = $(row.insertCell(-1));
        //cell.html(RankId.val());

        ////Add Button cell.
        //cell = $(row.insertCell(-1));
        //var btnRemove = $("<input />");
        //btnRemove.attr("type", "button");
        //btnRemove.attr("class", "btn btn-danger");
        //btnRemove.attr("onclick", "Remove(this);");
        //btnRemove.val("Xóa");
        //cell.append(btnRemove);

        var html = '';
        html += '<tr>';
        html += '<td>' + Id.val() + '</td>';
        html += '<td>' + Name.val() + '</td>';
        html += '<td>' + DateOfBirth.val() + '</td>';
        html += '<td>' + Class.val() + '</td>';
        html += '<td>' + Major.val() + '</td>';
        html += '<td style="display:none">' + DepartmentId.val() + '</td>';
        html += '<td style="display:none">' + RankId.val() + '</td>';
        html += '<td>' + DepartmentName.text() + '</td>';
        html += '<td>' + RankName.text() + '</td>';
        html += '<td><input type="button" class="btn btn-danger" onclick ="Remove(this)" value="Xóa" /></td></tr>';

        $('#item_table').append(html);

        //Clear All the TextBoxes.
        $('input[type=text]').each(function () {

            $(this).val('');

        });

    }

    else {
        alert("Hãy điền thông tin");

    }

});

function Remove(button) {
    //Determine the reference of the Row using the Button.
    var row = $(button).closest("TR");
    var name = $("TD", row).eq(0).html();
    if (confirm("Bạn có chắc chắn muốn xóa: " + name + "?")) {
        //Get the reference of the Table.
        var table = $("#tblMembers")[0];

        //Delete the Table row using it's Index.
        table.deleteRow(row[0].rowIndex);
    }
};

$("body").on("click", "#btnSave", function () {
    //Loop through the Table rows and build a JSON array.
    var members = new Array();
    $("#tblMembers TBODY TR").each(function () {
        var row = $(this);
        var member = {};

        member.MemberId = row.find("TD").eq(0).html();
        member.DepartmentId = row.find("TD").eq(5).html();
        member.RankId = row.find("TD").eq(6).html();

        members.push(member);
    });

    //Send the JSON array to Controller using AJAX.
    $.ajax({
        type: "POST",
        url: '@Url.Action("InsertMembers", "Members")',
        data: JSON.stringify(members),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (message) {
            if (message == 'ok') {
                alert("Các thành viên đã được lưu");

                //Empty all temp data in tbody #item_table
                $("#item_table").empty();
            }
            else {
                alert("Mã sinh viên: " + message + " Đã có trong danh sách hoặc không tồn tại");
            }

        }
    });
});

$('#txtId').on('input change paste', function () {
    $.ajax({
        type: "post",
        url: '@Url.Action("GetSearchingData", "Members")',
        data: {
            SearchValue: $("#txtId").val()
        },
        success: function (result) {
            if ($.trim(result)) {
                $.each(result, function (index, value) {
                    $("#txtId").val(value.Id);
                    $("#txtName").val(value.Name);
                    $("#txtDateOfBirth").val(value.DateOfBirth);
                    $("#txtClass").val(value.Class);
                    $("#txtMajor").val(value.Major);
                });
            }
            else {

                $("#txtName").val('');
                $("#txtDateOfBirth").val('');
                $("#txtClass").val('');
                $("#txtMajor").val('');
            }
        },

        error: function () {

            $("#txtName").val('');
            $("#txtDateOfBirth").val('');
            $("#txtClass").val('');
            $("#txtMajor").val('');
        }
    });
});