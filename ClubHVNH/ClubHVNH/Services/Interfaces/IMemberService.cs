﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubHVNH.Services.Interfaces
{
    public interface IMemberService
    {
        IEnumerable<MemberViewModel> GetMembersList(long clubid);
        IEnumerable<MemberViewModel> GetMembersInfo(long clubid, string Id);
        IQueryable GetMemberByPrefix(string Prefix);
        IQueryable GetMemberById(string Id);
        void UpdateMember(string memberid, long clubid, long? departmentid, long? rankid);
    }
}
