﻿using ClubHVNH.Models;
using ClubHVNH.Models.ViewModel;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClubHVNH.Services
{
    public class MemberService : IMemberService
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();

        public IEnumerable<MemberViewModel> GetMembersList(long clubid)
        {

           //Searching records from list using LINQ query  
            var memberList = (from j in db.JoinClubs.AsEnumerable()
                             join m in db.Members.AsEnumerable() on j.MemberId equals m.Id
                             join d in db.Departments.AsEnumerable() on j.DepartmentId equals d.Id
                             join r in db.Ranks.AsEnumerable() on j.RankId equals r.Id

                             where j.ClubId == clubid

                             select new MemberViewModel()
                             {
                                 Id = m.Id,
                                 Name = m.Name,                                
                                 DateOfBirth = m.DateOfBirth,
                                 Class = m.Class,
                                 Major = m.Major,
                                 Phone = m.Phone,
                                 Email = m.Email,
                                 CreatedDate = j.CreatedDate,
                                 DepartmentName = d.Name,
                                 RankName = r.Name,
                                 RankId = r.Id
                             }).OrderBy(x => x.RankId);

            return memberList.ToList();
        }

        public IEnumerable<MemberViewModel> GetMembersInfo(long clubid, string Id)
        {
            //Searching records from list using LINQ query  
            var member = from j in db.JoinClubs.AsEnumerable()
                             join m in db.Members.AsEnumerable() on j.MemberId equals m.Id
                             join d in db.Departments.AsEnumerable() on j.DepartmentId equals d.Id
                             join r in db.Ranks.AsEnumerable() on j.RankId equals r.Id

                             where j.ClubId == clubid && j.MemberId == Id

                             select new MemberViewModel()
                             {
                                 Id = m.Id,
                                 Name = m.Name,
                                 DateOfBirth = m.DateOfBirth,
                                 Class = m.Class,
                                 Major = m.Major,
                                 Phone = m.Phone,
                                 Email = m.Email,
                                 CreatedDate = j.CreatedDate,
                                 DepartmentName = d.Name,
                                 RankName = r.Name
                             };

            return member.ToList();
        }

        public IQueryable GetMemberByPrefix(string Prefix)
        {
            //Searching records from list using LINQ query  
            var memberList = from N in db.Members
                             where N.Id.StartsWith(Prefix)
                             select new 
                             {
                                 N.Id,
                                 N.Name,
                                 N.Class,
                                 N.DateOfBirth,
                                 N.Major
                             };

            return memberList;
        }

        public IQueryable GetMemberById(string Id)
        {
            //Searching records from list using LINQ query  
            var memberList = from N in db.Members
                             where N.Id == Id
                             select new 
                             {
                                 N.Id,
                                 N.Name,
                                 N.Class,
                                 N.DateOfBirth,
                                 N.Major
                             };

            return memberList;
        }

        public void UpdateMember(string memberid, long clubid, long? departmentid, long? rankid)
        {
            (from u in db.JoinClubs where u.MemberId == memberid && u.ClubId == clubid select u).ToList().ForEach(x =>
            {
                x.DepartmentId = departmentid;
                x.RankId = rankid;
            });

            db.SaveChanges();
        }

    }
}