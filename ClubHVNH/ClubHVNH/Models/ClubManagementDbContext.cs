namespace ClubHVNH.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ClubManagementDbContext : DbContext
    {
        public ClubManagementDbContext()
            : base("name=ClubManagement")
        {
        }

        public virtual DbSet<Club> Clubs { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<JoinClub> JoinClubs { get; set; }
        public virtual DbSet<JoinProgramDetail> JoinProgramDetails { get; set; }
        public virtual DbSet<JoinProgramList> JoinProgramLists { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<Program> Programs { get; set; }
        public virtual DbSet<Rank> Ranks { get; set; }
        public virtual DbSet<RankingCategory> RankingCategories { get; set; }
        public virtual DbSet<RankingList> RankingLists { get; set; }
        public virtual DbSet<RankingListDetail> RankingListDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostType> PostTypes { get; set; }
        public virtual DbSet<ReadPost> ReadPosts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JoinClub>()
                .Property(e => e.MemberId)
                .IsUnicode(false);

            modelBuilder.Entity<JoinProgramDetail>()
                .Property(e => e.MemberId)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.Class)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<RankingListDetail>()
                .Property(e => e.MemberId)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);
        }

        public System.Data.Entity.DbSet<ClubHVNH.Models.ViewModel.MemberViewModel> MemberViewModels { get; set; }
    }
}
