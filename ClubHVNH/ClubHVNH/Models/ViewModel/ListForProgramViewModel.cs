﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class ListForProgramViewModel
    {
        [Display(Name = "Mã sinh viên")]
        public string Id { get; set; }

        [Display(Name = "Họ và tên")]
        public string Name { get; set; }

        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Tên ban")]
        public string DepartmentName { get; set; }

        [Display(Name = "Chức vụ")]
        public string RankName { get; set; }

        [Display(Name = "Lớp")]
        public string Class { get; set; }

        [Display(Name = "Khoa")]
        public string Major { get; set; }

        public long PostId { get; set; }

        public long? JoinProgramListId { get; set; }

        [Display(Name = "Trạng thái tham gia")]
        public byte? JoinedOrNot { get; set; }
    }
}