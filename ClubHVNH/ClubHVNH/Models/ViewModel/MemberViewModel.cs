﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class MemberViewModel
    {
        [Display(Name = "Mã sinh viên")]
        public string Id { get; set; }

        [Display(Name = "Họ và tên")]
        public string Name { get; set; }

        [Display(Name = "Ngày sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Lớp")]
        public string Class { get; set; }

        [Display(Name = "Khoa")]       
        public string Major { get; set; }

        [Display(Name = "SĐT")]        
        public string Phone { get; set; }
       
        public string Email { get; set; }

        [Display(Name = "Ngày tham gia")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Tên ban")]
        public string DepartmentName { get; set; }

        [Display(Name = "Chức vụ")]
        public string RankName { get; set; }

        public long? RankId { get; set; }

        [Display(Name = "Số lần đã huy động")]
        public int NumberOfJoinProgram { get; set; }
    }
}