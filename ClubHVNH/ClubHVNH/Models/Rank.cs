﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("Rank")]
    public partial class Rank
    {
        public long Id { get; set; }

        [Display(Name = "Tên chức vụ")]
        [StringLength(250)]
        public string Name { get; set; }

        public long? ClubId { get; set; }
    }
}
