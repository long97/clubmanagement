﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("User")]
    public partial class User
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Hãy nhập tên đăng nhập")]
        [StringLength(64, ErrorMessage = "Tên đăng nhập phải trong khoảng 3-64 ký tự", MinimumLength = 3)]
        [Display(Name = "Tên đăng nhập")]
        [Remote("IsUserExists", "Users", ErrorMessage = "Tài khoản này đã có")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Hãy nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        [StringLength(50)]
        public string Password { get; set; }

        [Display(Name = "Là quản trị")]
        public bool IsAdmin { get; set; }

        [Required(ErrorMessage = "Hãy nhập Email")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(250)]
        public string Email { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Ngày sửa")]
        public DateTime? ModifiedDate { get; set; }

        [Display(Name = "Câu lạc bộ")]
        public long? ClubId { get; set; }
    }
}
