﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PostType")]
    public partial class PostType
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Hãy nhập tên loại thông báo")]
        [Display(Name = "Loại thông báo")]
        [StringLength(250)]
        public string Name { get; set; }

    }
}