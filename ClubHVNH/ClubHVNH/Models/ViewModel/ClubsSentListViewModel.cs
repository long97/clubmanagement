﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class ClubsSentListViewModel
    {
        public long? ClubId { get; set; }

        [Display(Name = "Tên câu lạc bộ")]
        public string ClubName { get; set; }

        [Display(Name = "Ngày gửi danh sách")]
        public DateTime? CreatedDate { get; set; }

        public DateTime? Deadline { get; set; }

        public long? SentListId { get; set; }

        public long? PostTypeId { get; set; }
    }
}