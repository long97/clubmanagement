﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClubHVNH.Models.ViewModel
{
    public class UserViewModel
    {
        public long? Id { get; set; }

        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        [Display(Name = "Là quản trị")]
        public bool IsAdmin { get; set; }

        public long? ClubId { get; set; }

        [Display(Name = "Câu lạc bộ")]
        public string ClubName { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Ngày sửa")]
        public DateTime? ModifiedDate { get; set; }
    }
}