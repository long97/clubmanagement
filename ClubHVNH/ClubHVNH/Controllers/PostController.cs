﻿using ClubHVNH.Models;
using ClubHVNH.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ClubHVNH.Controllers
{
    public class PostController : Controller
    {
        private ClubManagementDbContext db = new ClubManagementDbContext();
        private readonly IPostService postService;

        //inject dependency
        public PostController(IPostService postService)
        {
            this.postService = postService;
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        // GET: ProgramNotification
        public ActionResult Index()
        {
            return View(postService.GetPostsList());
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Create()
        {
            ViewBag.PostTypeId = new SelectList(db.PostTypes.ToList(), "Id", "Name");
            return View();
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id, Name, Description, Deadline, PostTypeId")] Post post)
        {
            if (ModelState.IsValid)
            {
                var createdate = DateTime.Now;
                post.CreatedDate = createdate;

                db.Posts.Add(post);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(post);
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var post = postService.GetPostInfo(id);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(post.FirstOrDefault());
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var post = db.Posts.Find(id);

            ViewBag.PostTypeId = new SelectList(db.PostTypes.ToList(), "Id", "Name", post.PostTypeId);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(post);
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id, Name, Description, Deadline, PostTypeId")] Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(post);
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var post = postService.GetPostInfo(id);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(post.FirstOrDefault());
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Post post = db.Posts.Find(id);

            db.Posts.Remove(post);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult NotificationDetails(long? id)
        {
            var clubid = (long)Session["clubid"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var post = postService.GetNotificationDetails(id, clubid);

            if (post == null)
            {
                return HttpNotFound();
            }

            ReadPost readPost = new ReadPost();

            readPost.PostId = Convert.ToInt32(post.Select(x => x.Id).FirstOrDefault());
            readPost.UserId = (long)Session["userid"];
            readPost.ReadTime = DateTime.Now;

            var readPostId = db.ReadPosts.Find(readPost.PostId, readPost.UserId);

            if(readPostId == null)
            {
                db.ReadPosts.Add(readPost);
                db.SaveChanges();
            }
            
            return View(post.FirstOrDefault());
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult ListClubSent(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var list = postService.GetClubsSentList(id);

            if (list == null)
            {
                return HttpNotFound();
            }

            return View(list.ToList());
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult ListMemberForProgram(long? id)
        {
            return View(postService.GetListMemberForProgram(id));
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        public ActionResult ListMemberForRanking(long? listid, long? clubid)
        {
            return View(postService.GetListMemberForRanking(listid, clubid));
        }

        [AuthorizeUser(AccessLevel = "HSV")]
        [HttpPost]
        public JsonResult UpdateJoinedOrNot(List<JoinProgramDetail> joinProgramDetails)
        {
            string message = "ok";

            //Check for NULL.
            if (joinProgramDetails == null)
            {
                joinProgramDetails = new List<JoinProgramDetail>();
            }

            //Loop and insert records.
            foreach (var joinProgramDetail in joinProgramDetails)
            {
                var memberIsValid = db.Members.Find(joinProgramDetail.MemberId);

                if (memberIsValid == null)
                {
                    message = joinProgramDetail.MemberId;
                    break;
                }
                
                var memberId = joinProgramDetail.MemberId;
                var joinedOrNot = Convert.ToByte(joinProgramDetail.JoinedOrNot);
                var joinProgramListId = joinProgramDetail.JoinProgramListId;

                var list = db.JoinProgramDetails.Where(x => x.MemberId == memberId && x.JoinProgramListId == joinProgramListId).SingleOrDefault();

                if(list != null)
                {
                    list.JoinedOrNot = joinedOrNot;
                    db.Entry(list).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }

            return Json(message);
        }
    }
}