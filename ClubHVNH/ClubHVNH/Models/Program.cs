﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Program")]
    public partial class Program
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Hãy nhập tiêu đề")]
        [Display(Name = "Tiêu đề")]
        [StringLength(250)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Hãy nhập nội dung")]
        [Display(Name = "Nội dung")]
        [StringLength(2000)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Hãy nhập thời gian tổ chức")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Thời gian tổ chức")]
        public DateTime? DateOrganized { get; set; }

        [Required(ErrorMessage = "Hãy nhập địa điểm")]
        [Display(Name = "Địa điểm")]
        [StringLength(250)]
        public string Location { get; set; }

        [Required(ErrorMessage = "Hãy nhập Deadline nộp danh sách")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Hạn nộp danh sách")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { get; set; }
    }
}
