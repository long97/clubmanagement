namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JoinProgramList")]
    public partial class JoinProgramList
    {
        public long Id { get; set; }

        public long? ClubId { get; set; }

        public long? PostId { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(250)]
        public string Note { get; set; }
    }
}
