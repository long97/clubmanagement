﻿namespace ClubHVNH.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("Department")]
    public partial class Department
    {
        public long Id { get; set; }

        [Display(Name = "Tên ban")]
        [StringLength(250)]
        public string Name { get; set; }

        [Display(Name = "Mô tả")]
        [StringLength(250)]
        public string Description { get; set; }

        public long? ClubId { get; set; }
    }
}
